use std::io::{self, Write};
use std::fs;
use std::process;
use clap::Parser;
use lalrpop_util::lalrpop_mod;

lalrpop_mod!(pub escpos);

#[cfg_attr(not(test), allow(unused_macros))]
macro_rules! test_expr {
    ($expr:expr, $expectation:expr) => {
        println!("parsing {}", stringify!($expr));
        let result = escpos::DocParser::new()
            .parse($expr)
            .unwrap();
        println!("result {:?}", result);
        assert_eq!(result, $expectation);
    };
}

#[cfg_attr(not(test), allow(unused_macros))]
macro_rules! test_err {
    ($expr:expr) => {
        println!("parsing {}", stringify!($expr));
        let result = escpos::DocParser::new()
            .parse($expr);
        assert!(result.is_err());
    };
}

#[test]
fn escpos_oct() {
    let result = escpos::DocParser::new().parse("0o10, 0o07");
    println!("{:?}", result);
    test_expr!("0o12", [10]);
    test_expr!("0o12 0o07 0o10", [10, 7, 8]);
}

#[test]
fn escpos_dec() {
    test_expr!("42", [42]);
    test_expr!("12 23 22", [12, 23, 22]);
    test_expr!("255", [255]);
    test_err!("256");
    test_err!("123014");
}

#[test]
fn escpos_hex() {
    test_expr!("0x42", [0x42]);
    test_expr!("0x42 0x12", [0x42, 0x12]);
    test_expr!("0x42 0x00", [0x42, 0x00]);
}

#[test]
fn escpos_sym() {
    test_expr!("ESC", [0x1B]);
    test_expr!("ESC ESC ESC", [0x1B, 0x1B, 0x1B]);
    test_expr!("NUL ESC EM SYN", [0x00, 0x1B, 25, 22]);
    test_err!("ZOP");
}

#[test]
fn escpos_lit() {
    test_expr!("\"0123456789\"", [48, 49, 50, 51, 52, 53, 54, 55, 56, 57]);
    test_expr!("\"ABCDEFGHIJKLMNOPQRSTUVWXYZ\"", [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90]);
    test_expr!("\"abcdefghijklmnopqrstuvwxyz\"", [65+32, 66+32, 67+32, 68+32, 69+32, 70+32, 71+32, 72+32, 73+32, 74+32, 75+32, 76+32, 77+32, 78+32, 79+32, 80+32, 81+32, 82+32, 83+32, 84+32, 85+32, 86+32, 87+32, 88+32, 89+32, 90+32]);

    test_expr!("\"Hello\"", [72, 101, 108, 108, 111]);
    test_expr!("\"Hello world\"", [72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]);
    test_expr!("\"Hello world!\"", [72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33]);
    test_expr!("\" !#$%&'()*+,-./\"", [32, 33, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47]);
    test_expr!("\":;<=>?@\"", [58, 59, 60, 61, 62, 63, 64]);
    test_expr!("\"[\\]^_`\"", [91, 92, 93, 94, 95, 96]);
    test_expr!("\"{|}~\"", [123, 124, 125, 126]);
}

#[test]
fn escpos_all() {
    let source = "
    ESC \"@\"
    ESC 1 2 0x12 200
    LF
    \"Hello world!\"
    LF
    ";
    let values = [
        0x1b, 0x40, 
        0x1b, 0x01, 0x02, 0x12, 200,
        0x0A,
        72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33,
        0x0A
    ];
    test_expr!(source, values);
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Input file
    input_file: Option<String>,
    /// Output file
    #[arg(long, short)]
    output_file: Option<String>,
}

fn write_output(out_file: Option<String>, out_bytes: Vec<u8>) -> Result<(), std::io::Error> {
    match out_file {
        None => {
            let stdout = io::stdout();
            let mut handle = stdout.lock();
            handle.write_all(&out_bytes)
        },
        Some(fname) => {
            fs::write(fname, out_bytes)
        },
    }
}


fn read_input(in_file: Option<String>) -> Result<String, std::io::Error> {
    match in_file {
        Some(fname) => {
            fs::read_to_string(fname)
        }, 
        None => {
            Err(std::io::Error::new(std::io::ErrorKind::Unsupported, "Reading stdin unsupported (programmer lazy)"))
        }
    }
}

fn main() {
    let args = Args::parse();

    let in_str = read_input(args.input_file);
    let code = match in_str {
        Ok(in_str) => {
            escpos::DocParser::new()
                .parse(&in_str)
                .unwrap()
        },
        Err(e) => {
            eprintln!("Error! {}", e);
            process::exit(1)
        }
    };
    let write_result = write_output(args.output_file, code);
    match write_result {
        Ok(_) => {
        },
        Err(e) => {
            eprintln!("Error! {}", e);
            process::exit(2)
        }
    }
}
